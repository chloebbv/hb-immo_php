<?php


use Bramus\Router\Router;

$router = new Router();
$router->setNamespace('App\Controller');

/**
 * On étbalit les différentes routes de l'applicationF
 */

$router->get('/', 'AppController@index');
$router->get('/asset', 'AssetController@index');
$router->get('/asset/(\d+)', 'AssetController@show');
$router->get('/asset/create', 'AssetController@create');
$router->post('/asset', 'AssetController@new');
$router->post('/asset/(\d+)', 'ContactController@new');

$router->run();

?>
