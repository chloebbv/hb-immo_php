<?php

/**
 * Configuration du projet
 */
const BASE_PATH = "http://localhost:8280/cours_php/hbimmo_chloe";

/**
 * Configuration de la base de données
 */
const DB_NAME = "hbimmo_chloe";
const DB_HOST = "localhost";
const DB_PORT = "3306";
const DB_USERNAME = 'root';
const DB_PASSWORD = '';

const DB_DSN = 'mysql:dbname='.DB_NAME.';host='.DB_HOST.';port='.DB_PORT.';charset=utf8';

?>