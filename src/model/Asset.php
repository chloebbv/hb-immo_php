<?php

namespace App\Model;

use PDO;

class Asset extends AbstractModel
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var float
     */
    private $value;
    /**
     * @var float
     */
    private $area;
    /**
     * @var int
     */
    private $rooms;
    /**
     * @var string
     */
    private $zipcode;
    /**
     * @var string
     */
    private $city;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * @param float $value
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
    /**
     * @return float
     */
    public function getArea()
    {
        return $this->area;
    }
    /**
     * @param float $area
     * @return self
     */
    public function setArea($area)
    {
        $this->area = $area;
        return $this;
    }
    /**
     * @return int
     */
    public function getRooms()
    {
        return $this->rooms;
    }
    /**
     * @param int $rooms
     * @return self
     */
    public function setRooms($rooms)
    {
        $this->rooms = $rooms;
        return $this;
    }
    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }
    /**
     * @param string $zipcode
     * @return self
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
        return $this;
    }
    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Récupère tous les assets
     * @return Array contenant tous les assets en BDD
     */
    public static function findAll()
    {
        $bdd = self::getPdo();
        $query = 'SELECT * FROM asset';
        $response = $bdd->prepare($query);
        $response->execute();
        return $response->fetchAll(PDO::FETCH_ASSOC);
    }
    /**
     * Récupère un asset par son id
     * @param id l'id de l'asset à récupérer
     * @return Asset l'objet Asset correspondant à l'id
     * 
     */

    public static function findOne($id)
    {
        $pdo = self::getPdo();
        $query = 'SELECT * FROM asset WHERE id = :id';
        $response = $pdo->prepare($query);
        $response->execute([
            'id' => $id,
        ]);
        $data = $response->fetch();
        $dataAsObject = self::toObject($data);
        return $dataAsObject;
    }
    /**
     * Transforme un array de données de la table Asset en un objet Asset
     */

    public static function toObject($array)
    {

        $asset = new Asset;
        $asset->setId($array['id']);
        $asset->setTitle($array['title']);
        $asset->setValue($array['value']);
        $asset->setArea($array['area']);
        $asset->setRooms($array['rooms']);
        $asset->setZipcode($array['zipcode']);
        $asset->setCity($array['city']);

        return $asset;
    }
    /**
     * Enregistre l'objet lui-même en base de données
     */
    public function store()
    {
        $pdo = self::getPdo();

        $query = 'INSERT INTO asset(title, value, area, rooms, zipcode, city) VALUES (:title, :value, :area, :rooms, :zipcode, :city)';

        $response = $pdo->prepare($query);
        $response->execute([
            'title' => $this->getTitle(),
            'value' => $this->getValue(),
            'area' => $this->getArea(),
            'rooms' => $this->getRooms(),
            'zipcode' => $this->getZipcode(),
            'city' => $this->getCity()
        ]);

        return true;
    }
}


?>