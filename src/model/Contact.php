<?php

namespace App\Model;

use PDO;

class Contact extends AbstractModel
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $firstname;
    /**
     * @var string
     */
    private $lastname;
    /**
     * @var string
     */
    private $email;
    private $message;
    /**
     * @var int
     */
    private $id_asset;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }
    /**
     * @param string $firstname
     * @return self
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }
    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }
    /**
     * @param string $lastname
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }
    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
    /**
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
    /**
     * @return int
     */
    public function getId_asset()
    {
        return $this->id_asset;
    }
    /**
     * @param int $id_asset
     * @return self
     */
    public function setId_asset($id_asset)
    {
        $this->id_asset = $id_asset;
    }

    /**
     * Récupère les contacts par l'asset_id
     *@param id id de l'asset pour lequel on réucupère les contacts
     *@return Array contenant les contacts
     */

    public static function findByAssetId($id)
    {
        $pdo = self::getPdo();
        $query = 'SELECT * FROM contact WHERE id_asset = :id';
        $response = $pdo->prepare($query);
        $response->execute([
            'id' => $id,
        ]);
        return $response->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Enregistre l'objet lui-même en base de données
     */
    public function store()
    {

        $pdo = self::getPdo();

        $query = 'INSERT INTO contact(firstname, lastname, email, message, id_asset) VALUES (:firstname, :lastname, :email, :message, :id_asset)';

        $response = $pdo->prepare($query);
        $response->execute([
            'firstname' => $this->getFirstname(),
            'lastname' => $this->getLastname(),
            'email' => $this->getEmail(),
            'message' => $this->getMessage(),
            'id_asset' => $this->getId_asset()
        ]);

        return true;
    }
}
?>