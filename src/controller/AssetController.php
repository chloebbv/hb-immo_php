<?php
namespace App\Controller;

use App\Model\Asset;
use App\Model\Contact;

class AssetController extends AbstractController{

    /**
     * Fonction qui appelle la fonction findAll() de la classe Asset
     * et qui affiche le return (tous les assets en BDD) dans la page -> /views/asset/index.html
     */
    public static function index() {
        $assets = Asset::findAll();
        echo self::getTwig()->render('asset/index.html', ['assets' => $assets]);
        
    }

    /**
     * Fonction:
     * - qui appelle la fonction findOne() de la classe Asset et qui affiche le return (l'asset dont l'id est passé en paramètres) dans la page 
     * - qui appelle la fonction findByAssetId() de la classe Asset et qui affiche le return (les contacts pour l'asset dont l'id est passé en paramètres) dans la page 
     * 
     * -> /views/asset/show.html
     * @param id id de l'asset à affiché et pour lequel on récupère les commentaires
     */
    public static function show($id) {
        $asset = Asset::findOne($id);
        $contacts = Contact::findByAssetId($id);
        echo self::getTwig()->render('asset/show.html', [
            'asset' => $asset,
            'contacts' => $contacts
        ]);
        
    }

    /**
     * Fonction qui affiche la page de création d'un asset -> /views/app/create.html
     */
    public static function create() {
        echo self::getTwig()->render('asset/create.html');
    }

    /**
     * Fonction qui crée un nouvel asset et appel la fonction store() de la classe Asset pour l'ajouter en BDD
     * et rappelle la méthode index() d'AssetController
     */
    public static function new() {
        $animal = new Asset;
        $animal->setTitle($_POST['title']);
        $animal->setValue($_POST['value']);
        $animal->setArea($_POST['area']);
        $animal->setRooms($_POST['rooms']);
        $animal->setZipcode($_POST['zipcode']);
        $animal->setCity($_POST['city']);
        $animal->store();

        self::index();
    }


}

?>
