<?php
namespace App\Controller;

use App\Model\Contact;

class ContactController extends AbstractController{


    /**
     * Fonction qui crée un nouveau contact et appel la fonction store() de la classe Contact pour l'ajouter en BDD
     * et rappelle la méthode show() d'AssetController pour afficher le commentaire ajouté sur la page de détail de l'asset
     * @param id id de l'asset concerné par l'ajout de contact
     */
    public static function new($id) {
        $contact = new Contact;
        $contact->setLastname($_POST['lastname']);
        $contact->setFirstname($_POST['firstname']);
        $contact->setEmail($_POST['email']);
        $contact->setMessage($_POST['message']);
        $contact->setId_asset($id);
        $contact->store();

        AssetController::show($id);
    }


}
?>