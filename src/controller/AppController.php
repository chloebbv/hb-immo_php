<?php

namespace App\Controller;

class AppController extends AbstractController {

    /**
     * Fonction qui affiche la page d'accueil -> /views/app/index.html
     */
    public static function index() {
        echo self::getTwig()->render('app/index.html');
    }

}
?>