-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 17 juin 2020 à 14:18
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `hbimmo_chloe`
--

-- --------------------------------------------------------

--
-- Structure de la table `asset`
--

DROP TABLE IF EXISTS `asset`;
CREATE TABLE IF NOT EXISTS `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `area` decimal(10,2) NOT NULL,
  `rooms` int(11) NOT NULL,
  `zipcode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `asset`
--

INSERT INTO `asset` (`id`, `title`, `value`, `area`, `rooms`, `zipcode`, `city`) VALUES
(1, 'Maison plein Sud', '300000.00', '150.00', 6, '38200', 'Vienne'),
(2, 'Appartement plein de charme', '150000.00', '44.00', 1, '69007', 'Lyon'),
(3, 'Test', '100.00', '10.00', 1, '69000', 'Lyon');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `id_asset` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_asset` (`id_asset`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `firstname`, `lastname`, `email`, `message`, `id_asset`) VALUES
(1, 'Jean-Jacques', 'Dupont', 'jjdupont@mail.com', 'Bonjour, je souhaiterai obtenir plus de renseignements concernant cette grande maison.\r\nPouvez-vous me contacter par mail svp?', 1),
(2, 'Jeanne', 'Martin', 'jeanne.martin@mail.com', 'Contactez-moi svp!!', 2),
(3, 'Jean-Luc', 'Reichmann', 'jlr@tf1.fr', 'Pouvez-vous me rappeler au \r\n0600000000?\r\nMerci.', 1),
(4, 'Claire', 'Chazal', 'cchazal@france5.fr', 'Bonjour,\r\n\r\nCe bien est-il éligible à un programme de défiscalisation?\r\n\r\nMerci par avance pour votre retour', 2),
(7, 'Chloé', 'Bui-Van', 'chloe.buivan@gmail.com', 'Ceci est un autre test', 1),
(5, 'Chloé', 'Bui-Van', 'chloe.buivan@gmail.com', 'Ceci est un test', 2),
(9, 'Chloé', 'Bui-Van', 'chloe.buivan@gmail.com', 'Dernier test', 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
